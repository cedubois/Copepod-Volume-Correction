import numpy as np
from scipy.stats import gaussian_kde
from utils import plot_ratio_pdf, fit_pdf_kde, size_spectrums
import ellipsoid_projection as E
import matplotlib.pyplot as plt
#

if __name__ == "__main__":
    Maj = np.load("./res-axes-measures/Maj.npy")
    Min = np.load("./res-axes-measures/Min.npy")
    pixel = np.load("./res-axes-measures/Pixel.npy")
    top = np.load("./res-axes-measures/top.npy")
    side = np.load("./res-axes-measures/side.npy")
    alll = np.load("./res-axes-measures/all.npy")
    #
    # r1 = Maj[top]*pixel[top] / 2
    r1 = np.concatenate((Maj[side]*pixel[side], Maj[top]*pixel[top])) / 2
    print(f"r1 min : {np.min(r1)}")
    # r1 = r1[r1<2]
    r1_pdf = gaussian_kde(r1)
    # x = np.linspace(0, 6, 100)
    # plt.plot(x, r1_pdf(x))
    # plt.show()
    #
    r2_over_r1 = Min[side] / Maj[side]
    r2_pdf = gaussian_kde(r2_over_r1)
    # plot_ratio_pdf(r2_over_r1, r2_pdf, label="$r_2/r_1$")
    #
    r3_over_r1 = Min[top] / Maj[top]
    r3_pdf = gaussian_kde(r3_over_r1)
    # plot_ratio_pdf(r3_over_r1, r3_pdf, label="$r_3/r_1$", color='royalblue')
    #
    N = int(1e4)
    ec = E.Projection(N=N)
    ec.proj_set_scale_invariant(r2_pdf=r2_pdf,r3_pdf=r3_pdf, r1_pdf_shape='exp', r1_pdf=r1_pdf)
    alpha = 1 / ec.r1**3
    # alpha = np.random.uniform(100,200, size=ec.N)
    volume = ec.Vr
    volume_norm = ec.Vr * alpha
    volume_star = ec.V_Ell
    beta = volume_star / volume

    data = np.vstack((alpha, volume))
    data_star = np.vstack((alpha, volume_star))

    r2_samples = ec.r2*ec.r1
    r3_samples = ec.r3*ec.r1
    print(ec.r1.min(), ec.r1.max())
    print(f"Mean volume {np.mean(volume)}")



    # print(np.corrcoef(alpha*volume, alpha*volume_star))
    # print(np.corrcoef(volume, volume_star))
    #
    # plt.scatter(ec.V_ESD, ec.Vr, s=1)
    # poly = np.polyfit(ec.V_ESD, ec.Vr, deg=1)
    # print(poly)
    # x = np.linspace(1e-3,10, 1000)
    # plt.plot(x, np.poly1d(poly)(x), c='k', ls='--')
    # plt.plot(x, poly[0]*x, c='k', ls='-')
    # plt.plot(x, x, c='gray', ls='-')
    #
    #
    # plt.hist(ec.r1, bins=25)
    # Bin, bss_norm, bss = size_spectrums(ec.Vr)
    # plt.plot(Bin, bss_norm)
    # Bin, bss_norm, bss = size_spectrums(ec.V_ESD)
    # plt.plot(Bin, bss_norm)
    # Bin, bss_norm, bss = size_spectrums(ec.V_Ell)
    # plt.plot(Bin, bss_norm)
    # print(ec.r1.min())
    # plt.hist(ec.r1, bins=50)
    # plt.show()
    # #
    # print(f"Var_ESD = {np.var(ec.V_ESD / ec.Vr)}",
    #       f"Var_ELL = {np.var(ec.V_Ell / ec.Vr)}",
    #       f"Var_alpha = {np.var(alpha)}",
    #         sep="\n")
    # plt.hist(alpha, bins=50)
    # plt.show()
    # print(np.mean(alpha))
    # plt.hist(ec.Vr, bins=50)
    # plt.show()

    # plt.scatter(Maj[side]*pixel[side], Min[side]*pixel[side], s=1)
    # plt.scatter(Maj[top]*pixel[top], Min[top]*pixel[top], s=1)
    # plt.show()
    # sub_sample = np.random.choice(range(0, N), size=500)
    # r2_m = r2_samples[sub_sample]
    # r1_m_r2 = ec.r1[sub_sample]
    # sub_sample = np.random.choice(range(0, N), size=500)
    # r3_m = r3_samples[sub_sample]
    # r1_m_r3 = ec.r1[sub_sample]
    #
    # volume_m = np.mean((r1_m_r2, r1_m_r3))*np.mean(r2_m)*np.mean(r3_m)*np.pi*4/3
    # # volume_m = np.mean(r2_samples)*np.mean(r3_samples)*np.mean(ec.r1) * np.pi *  4 / 3
    # # volume_m = np.mean(r2_samples*r3_samples*ec.r1) * np.pi * 4 / 3
    # print(f"mean volume from axes : {volume_m}")
    # print(f"real mean volume ESD : {np.mean(ec.V_ESD)}")
    # print(f"real mean volume ELL : {np.mean(ec.V_Ell)}")
    # print(f"real mean volume : {np.mean(volume)}")
    # print(f"ratio {volume_m / np.mean(volume)}")

    # corr = volume_star / volume
    # plt.hist2d(np.log10(volume_star), corr, bins=100, )
    # kde_corr = gaussian_kde(corr)(corr)
    # plt.scatter(ec.Rho1*ec.Rho2**2, corr, s=1, c=kde_corr)
    # plt.show()

    # print(f"E[alpha V] : \n {np.mean(alpha * volume)}", )
    # print(f"E[alpha V*] : \n  {np.mean(alpha * volume_star)}", )

    # print(1 / np.mean(volume))
    # print(1 / np.mean(volume_star))
    # print(np.mean(alpha))

    # plt.hist(volume, bins=50)
    # plt.show()

    # data_2 = np.vstack((beta, volume))
    # print(f"{np.mean(beta*volume_star)*np.mean(alpha*volume) / np.mean(volume) * np.mean(alpha*beta*volume)}")
    # print(f"{np.mean(beta*volume)*np.mean(alpha*volume) - np.mean(volume) * np.mean(alpha*beta*volume)}")
    # print(f"{np.mean(volume_star) - np.mean(beta)*np.mean(volume)}")

    # print(f"E[alpha V] / E[V] : \n {np.mean(alpha*volume) / np.mean(volume)}", )
    # print(f"E[alpha V*] / E[V*] : \n  {np.mean(alpha*volume_star) / np.mean(volume_star)}", )
    # print(f"diff : {np.mean(alpha*volume) / np.mean(volume) - np.mean(alpha*volume_star) / np.mean(volume_star)} ")
    # print(f"Product {np.mean(alpha*volume) * np.mean(volume_star) / np.mean(volume) / np.mean(alpha*volume_star)}")

    print(f"cov : \n {np.cov(data) }", )
    print(f"corr : \n {np.corrcoef(data) }", )
    print(f"cov* : \n  {np.cov(data_star) }", )
    print(f"corr* : \n  {np.corrcoef(data_star) }", )
    # print(f"cov / E : \n {np.cov(data) / np.mean(volume)}",)
    # print(f"cov* / E* : \n  {np.cov(data_star) / np.mean(volume_star)}",)

    # print(f"E[alpha] = {np.mean(alpha)}")

    # print(f"diff cov : {np.mean(volume)*np.mean(alpha*volume_star) - np.mean(alpha*volume)*np.mean(volume_star)}")
    # print(f"cov : \n {np.cov(volume, alpha*volume_star)} vs \n {np.cov(alpha*volume, volume_star)}")
    # print(f"cov : \n {np.cov(volume, alpha*volume_star) - np.covccovcovovcovcovcovcovcovcovcov(alpha*volume, volume_star)}")

    # print("Ratios E ELL" ,
    #       f"{np.mean(volume_star) / np.mean(volume)}",
    #       f"{np.mean(volume_star * alpha) / np.mean(volume_norm)}",
    #       f"{np.mean(volume_star / volume)}",
    #       f"{np.mean(alpha*volume_star / volume_norm)}",
    #       sep="\n")
    print(f"Tau_ELL = {np.mean(volume_star) / np.mean(volume)}",
        f"ELL : \n Ratio normalized / true: {100 * np.mean(volume_star * alpha) / np.mean(alpha*volume) / (np.mean(volume_star) / np.mean(volume))}",
         )
    print("-" * 30)

    volume_star = ec.V_ESD
    # print("Ratios E ESD" ,
    #       f"{np.mean(volume_star) / np.mean(volume)}",
    #       f"{np.mean(volume_star * alpha) / np.mean(volume_norm)}",
    #       f"{np.mean(volume_star / volume)}",
    #       f"{np.mean(alpha*volume_star / volume_norm)}",
    #       sep="\n")
    print(f"Tau_ESD = {np.mean(volume_star) / np.mean(volume)}",
        f"ESD : \n Ratio normalized / true: {100 * np.mean(volume_star * alpha) / np.mean(alpha * volume) / (np.mean(volume_star) / np.mean(volume))}",
        )
    print("-" * 30)


    # print("alpha vs volume (normed)",
    #     f"shape{data.shape}",
    #     f"corr : {np.corrcoef(data)}",
    #     f"cov / E : {np.cov(data) / np.mean(volume_norm)}",
    #     f"E[alpha]*E[V_norm] / E[alpha V_norm] : {np.mean(alpha)*np.mean(volume_norm) / np.mean(alpha*volume_norm)}",
    #      sep="\n")
    # #
    # print("RATIO",
    #     f"E[alpha V_norm]*E[V*] / ( E[V_norm]*E[alpha V*] ):"
    #     f" {np.mean(alpha*volume_norm)*np.mean(volume_star) / np.mean(volume_norm) / np.mean(alpha*volume_star)}",
    #      sep="\n")
    # print("-"*30)
    # #
    # print("-"*30)
    # data = np.vstack((alpha, volume))
    # print("alpha vs alpha volume",
    #     f"shape{data.shape}",
    #     f"corr : {np.corrcoef(data)}",
    #     f"cov / E : {np.cov(data) / np.mean(volume)}",
    #     f"E[alpha]*E[V] / E[alpha V]: {np.mean(alpha)*np.mean(volume) / np.mean(alpha*volume)}",
    #      sep="\n")
    # print("-"*30)
    # #
    # print("RATIO",
    #     f"E[alpha V]*E[V*] / ( E[V]*E[alpha V*] ): "
    #     f"{np.mean(alpha*volume)*np.mean(volume_star) / np.mean(volume) / np.mean(alpha*volume_star)}",
    #      sep="\n")
    # print("-"*30)
    #
    # print("RATIO 2" ,
    #       f" ): "
    #       f"{np.mean(volume_star) / np.mean(volume)}",
    #       f"{np.mean(volume_star / volume)}",
    #       sep="\n")
    # print("-" * 30)
