import numpy as np
from scipy.stats import gaussian_kde
import ellipsoid_projection as E
import matplotlib.pyplot as plt
from utils import plot_kde
import tikzplotlib
#

if __name__ == "__main__":
    Maj = np.load("./res-axes-measures/Maj.npy")
    Min = np.load("./res-axes-measures/Min.npy")
    pixel = np.load("./res-axes-measures/Pixel.npy")
    top = np.load("./res-axes-measures/top.npy")
    side = np.load("./res-axes-measures/side.npy")
    alll = np.load("./res-axes-measures/all.npy")
    #
    r2_over_r1 = Min[side] / Maj[side]
    r2_pdf = gaussian_kde(r2_over_r1)
    # plot_ratio_pdf(r2_over_r1, r2_pdf, label="$r_2/r_1$")
    #
    r3_over_r1 = Min[top] / Maj[top]
    r3_pdf = gaussian_kde(r3_over_r1)
    # plot_ratio_pdf(r3_over_r1, r3_pdf, label="$r_3/r_1$", color='royalblue')
    #
    N = int(1e3)
    ec = E.Projection(N=N)
    ec.proj_set_scale_invariant(r2_pdf=r2_pdf,r3_pdf=r3_pdf, r1_pdf_shape=None)
    err_ell = ec.V_Ell / ec.Vr
    err_esd = ec.V_ESD / ec.Vr
    #
    fig = plt.figure(figsize=(10,7.5),dpi=150)
    ax = plt.subplot()
    ax.hist(err_ell, bins=50, density=True, color='r', ec='k', alpha=0.5)
    plot_kde(err_ell, ax=ax, c='k', n=1000, lw=2)
    ax.set_xlabel("$\mathcal{M}_{ELL}$", fontsize=20)
    plt.xticks(fontsize=15)
    plt.yticks(fontsize=15)
    plt.show()
    #
    fig = plt.figure(figsize=(10, 7.5), dpi=150)
    ax = plt.subplot()
    ax.hist(err_esd, bins=50, density=True, color='b', ec='k', alpha=0.5)
    plot_kde(err_esd, ax=ax, c='k', n=1000, lw=2)
    ax.set_xlabel("$\mathcal{M}_{ESD}$", fontsize=20)
    plt.xticks(fontsize=15)
    plt.yticks(fontsize=15)
    plt.show()
    #
    # import plotly.express as px
    # fig = px.histogram(err_ell)
    # fig.show()