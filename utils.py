# Copyright UCA/CNRS/Inria
# Contributor(s): Cedric Dubois 2022
#
# cedric.dubois@inria.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter
from scipy.stats import gaussian_kde
import sys

def plot_kde(q, bw=None, n=100, ax=None, **kwargs):
    x = np.linspace(q.min(), q.max(), n)
    kde = gaussian_kde(q, bw_method=bw)
    if ax==None:
        plt.figure()
        plt.plot(x, kde.pdf(x), **kwargs)
    # elif isinstance(ax, Ax): # Todo
    else:
        ax.plot(x, kde.pdf(x), **kwargs)
def plot_density(x, y):
    if x.shape[0] > 10000:
        x = x[:10000]
        y = y[:10000]
    plt.figure(dpi=150)
    xy = np.vstack([x, y])
    z = gaussian_kde(xy)(xy)
    #
    plt.scatter(x, y, c=z, s=1)
    plt.show()


def plot_density_3D(x, y, z):
    if x.shape[0] > 10000:
        x = x[:1000]
        y = y[:1000]
        z = z[:1000]
    figure = plt.figure(dpi=150)
    axes = figure.add_subplot(projection="3d")
    xyz = np.vstack([x, y, z])
    c = gaussian_kde(xyz)(xyz)
    #
    cax = axes.scatter(x, y, z, c=c, s=1)
    figure.colorbar(cax)
    plt.show()

def plot_ratio_pdf(Q, r_pdf, label="$r_2/r_1$", color='mediumseagreen', ticks_size=20):
    x = np.linspace(0,1,100)
    y = r_pdf.pdf(x)
    delta = Q.max() - Q.min()
    B = int(np.sqrt(Q.shape[0]) / delta)
    n, bins = histo(Q, bins=B, log=False, density=True, Range=(0, 1))
    plt.figure(figsize=(10,7.5),dpi=300)
    plt.plot(bins, n, 's', lw=1, markersize=6, c='k')
    plt.plot(x,y,lw=4,c=color)
    plt.grid()
    plt.xticks(size=ticks_size)
    plt.yticks(size=ticks_size)
    plt.xlabel(label,fontsize=25)

    plt.show()
def progression(i,n):
    if(i%100 == 0):
        sys.stdout.write("\r{0} %".format(round((float(i)/n)*100,0)))
        sys.stdout.flush()

def fit_pdf_kde(x, show_plot=False, bw=None, xlabel=None, color='mediumseagreen'):
    """
    1-D Gaussian Kernel Density Estimation (G-KDE) of a given quantity x.
    Optional: Plot histogram and G-KDE.
    Automatically set the range and the number of bins for the histogram:
    range_ = x.max() - x.min()
    n_bins = int(np.sqrt(x.shape[0]) / range_)

    Parameters
    ----------
    @param x: array
        Quantity to estimate.
    @param show_plot: bool, optional.
        To show plot of the histogram and G-KDE.
    @param bw (from scipy.stats.gaussian_kde): str or scalar, optional
        The method used to calculate the estimator bandwidth.  This can be
        'scott', 'silverman', a scalar constant or a callable.  If a scalar,
        this will be used directly as `kde.factor`.
        If None (default), 'scott' is used.  See Notes for more details.

    @param xlabel: str, optional.
        to custom x-label on plot. Used only if show_plot is set to True.
        Default is None.

    @param color: str, optional.
        color line for the gaussian KDE plot. Used only if show_plot is set to True.
        Default is 'mediumseagreen'.

    Returns
    ------
    axis_ratio_pdf: scipy.stats.kde.gaussian_kde
        Gaussian KDE estimation.
    """
    axis_ratio_pdf = gaussian_kde(x, bw_method=bw)
    if show_plot:
        range_ = x.max() - x.min()
        n_bins = int(np.sqrt(x.shape[0]) / range_)
        plt.figure(figsize=(10, 7.5), dpi=300)
        n, bins = histo(x, bins=n_bins, log=False, density=True, Range=(0,1))
        plt.plot(bins, n, 'o', lw=1, markersize=6, c='k', marker='s')
        # plt.hist(x,bins=n_bins,density=True,alpha=0.4,facecolor=color,edgecolor='k',lw=2)
        x = np.linspace(0, 1, 100)
        y = axis_ratio_pdf.pdf(x)
        plt.plot(x, y, lw=4, c=color)
        plt.grid()
        # plt.plot(bins,n,'--o',lw=2,markersize=8,c='k',marker='s')
        # plt.tight_layout()
        plt.xticks(size=20)
        plt.yticks(size=20)
        if not xlabel == None:
            plt.xlabel(xlabel, fontsize=25)
    return axis_ratio_pdf

def histo(X, Range=(0,1), bins=50, log=True, density=True):
    """
    Basic histograms.
    Parameters:
    -----------
    @param X: array, quantity
    @param Range: tuple, range of the hist, default is (0,1)
    @param bins: int, number of bins, default is 50
    @param log: bool, log-scale or not, default is False
    @param density: bool, to normalize or not, default is True

    Returns:
    --------
    n: value of hist.
    Bins: center of the bins
    """
    if log is True:
        Range = np.log10(Range)
        n, bins = np.histogram(np.log10(X),density=density, bins=bins,range=Range)
    else:
        n, bins = np.histogram(X,density=density, bins=bins,range=Range)
                           #range=(np.log10(ec3.Vr).min(),np.log10(ec3.Vr).max()))
    Bins = np.zeros_like(n.astype('float32'))
    for i, ii in enumerate(n):
        Bins[i] = (bins[i + 1] + bins[i]) / 2.
    return n, Bins

def size_spectrums(x,BW=0.1,w=1,nass=False):
    x_log = np.log(x)
    #
    xx = x*w
    xx_log = np.log(xx)
    #
    N = x_log.shape[0]
#     x_log_bin = np.ndarray([N])
#     for i in range(N):
    x_log_bin = np.round(x_log/BW,0) * BW
    #
    all_bins = np.intersect1d(x_log_bin,x_log_bin)
    bss = np.ndarray([all_bins.shape[0]])
    i = 0
    if nass is True:
        for val in all_bins:
            bss[i] = np.where(x_log_bin==val)[0].shape[0]
            i += 1
    else:
        for val in all_bins:
            bss[i] = xx[np.where(x_log_bin==val)].sum()
            i += 1
    #
    Bin = np.exp(all_bins)
    BinWidth = np.exp(all_bins+BW/2) - np.exp(all_bins-BW/2)
    bss_norm = bss/BinWidth
    return Bin, bss_norm, bss

def plot_2dHist_prj(x,y,bins=50):
    nullfmt = NullFormatter()  # no labels

    # definitions for the axes
    left, width = 0.1, 0.65
    bottom, height = 0.1, 0.65
    bottom_h = left_h = left + width + 0.02

    rect_scatter = [left, bottom, width, height]
    rect_histx = [left, bottom_h, width, 0.2]
    rect_histy = [left_h, bottom, 0.2, height]

    # start with a rectangular Figure
    plt.figure(1, figsize=(8, 8))

    axScatter = plt.axes(rect_scatter)
    axHistx = plt.axes(rect_histx)
    axHisty = plt.axes(rect_histy)

    # no labels
    axHistx.xaxis.set_major_formatter(nullfmt)
    axHisty.yaxis.set_major_formatter(nullfmt)

    # the scatter plot:
    axScatter.hist2d(x, y,bins=bins,cmap='summer')

    #
    axScatter.set_xlabel("Latitude")
    axScatter.set_ylabel("Longitude")
    # now determine nice limits by hand:
    #binwidth = 0.25
    #xymax = np.max([np.max(np.fabs(x)), np.max(np.fabs(y))])
    #lim = (int(xymax / binwidth) + 1) * binwidth

    #axScatter.set_xlim((-lim, lim))
    #axScatter.set_ylim((-lim, lim))

    #bins = np.arange(-lim, lim + binwidth, binwidth)
    axHistx.hist(x, bins=bins)
    axHisty.hist(y, bins=bins, orientation='horizontal')

    axHistx.set_xlim(axScatter.get_xlim())
    axHisty.set_ylim(axScatter.get_ylim())

    plt.show()

