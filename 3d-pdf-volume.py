import numpy as np
from scipy.stats import gaussian_kde
import ellipsoid_projection as E
#
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
from matplotlib import cm#

if __name__ == "__main__":
    Maj = np.load("./res-axes-measures/Maj.npy")
    Min = np.load("./res-axes-measures/Min.npy")
    pixel = np.load("./res-axes-measures/Pixel.npy")
    top = np.load("./res-axes-measures/top.npy")
    side = np.load("./res-axes-measures/side.npy")
    alll = np.load("./res-axes-measures/all.npy")
    #
    r1 = np.concatenate((Maj[side]*pixel[side], Maj[top]*pixel[top])) / 2
    r1_pdf = gaussian_kde(r1)
    #
    r2_over_r1 = Min[side] / Maj[side]
    r2_pdf = gaussian_kde(r2_over_r1)
    # plot_ratio_pdf(r2_over_r1, r2_pdf, label="$r_2/r_1$")
    #
    r3_over_r1 = Min[top] / Maj[top]
    r3_pdf = gaussian_kde(r3_over_r1)
    # plot_ratio_pdf(r3_over_r1, r3_pdf, label="$r_3/r_1$", color='royalblue')
    #
    N = int(1e3)
    ec = E.Projection(N=N)
    ec.proj_set_scale_invariant(r2_pdf=r2_pdf,r3_pdf=r3_pdf, r1_pdf_shape='kde', r1_pdf=r1_pdf)
    volume = ec.Vr
    X = ec.r1
    Y = ec.r2*ec.r1
    Z = ec.r3*ec.r1
    xyz = np.vstack([X, Y, Z])
    dens = gaussian_kde(xyz)(xyz)
    #
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(X, Y, Z, s=0.5, alpha=1, c=dens)
    #
    n = 200
    ax.scatter(X[:n], Z[:n], c='mediumseagreen', zdir='y', zs=5, s=0.5)
    ax.scatter(X[n:2*n], Y[n:2*n], c='royalblue', zdir='z', zs=0, s=0.5)
    ax.scatter(Y[2*n:3*n], Z[2*n:3*n], c='orange', zdir='x', zs=0, s=0.5)


    ax.set_xlabel('$r_1$')
    ax.set_xlim(0, 5)
    ax.set_ylabel('$r_2$')
    ax.set_ylim(0, 5)
    ax.set_zlabel('$r_3$')
    ax.set_zlim(0, 3)

    plt.show()
