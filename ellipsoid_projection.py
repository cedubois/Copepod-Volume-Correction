# Copyright UCA/CNRS/Inria
# Contributor(s): Cedric Dubois 2022
#
# cedric.dubois@inria.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#
import warnings
import random
import numpy as np
from utils import progression
from tqdm import tqdm

#
def rot_x(a):
    cosa = np.cos(a)
    sina = np.sin(a)
    return np.array([[1, 0, 0], [0, cosa, -sina], [0, sina, cosa]])


def rot_y(a):
    cosa = np.cos(a)
    sina = np.sin(a)
    return np.array([[cosa, 0, sina], [0, 1, 0], [-sina, 0, cosa]])


def rot_z(a):
    cosa = np.cos(a)
    sina = np.sin(a)
    return np.array([[cosa, -sina, 0], [sina, cosa, 0], [0, 0, 1]])


def random_rotation(A, higher_limit_angle):
    """
    Random and uniform rotation of a matrix.
    @param A: Matrix of axis-aligned ellipsoid
    @param higher_limit_angle: float in [0,1]. 0 correspond to no rotation along y-axis.
    @return: Rotated matrix
    """
    # Sample rotation along z,y and x (note z is useless -> rotation in the image plane).
    # P = random.random() * 2 * np.pi # z-axis rotation
    # P = 0
    # Exact formula for Theta is:
    # T = np.arccos(1 - 2*random.random()) - np.pi / 2.
    # -> [- pi/2, pi/2]
    # But with symmetry we have:
    T = abs(np.arccos(random.uniform(0, higher_limit_angle)) - np.pi / 2.)
    # -> [0, pi/2]
    #
    R = random.random() * 2 * np.pi  # x-axis rotation
    # Rotation matrix:
    # rot = rot_x(R) @ rot_y(T) @ rot_z(P) # (useless)
    rot = rot_x(R) @ rot_y(T)
    return np.transpose(rot) @ A @ rot


def proj_prll_static(r1, r2, r3, higher_limit_angle=None):
    """
    Compute the parallel projection of an ellipsoid (r1,r2,r3) for a random orientation (P,T,R)

    Parameters:
    -----------
    @param r1: major semi-axe
    @param r2: first minor semi-axe
    @param r3: second minor semi-axe

    Returns:
    --------
    @return: roh1, roh2: projected axes
    """
    # Ellipsoid aligned on the axis:
    axis_aligned_mat = np.zeros([3, 3])
    axis_aligned_mat[0, 0] = 1. / r1 ** 2.
    axis_aligned_mat[1, 1] = 1. / r2 ** 2.
    axis_aligned_mat[2, 2] = 1. / r3 ** 2.
    #
    rotated_mat = random_rotation(axis_aligned_mat, higher_limit_angle)
    #
    A11 = rotated_mat[:2, :2]
    A21 = rotated_mat[2, :2].reshape(1, 2)
    r = - rotated_mat[2, 2]
    A21_T = np.transpose(A21)
    P = A21_T.dot(A21) + r * A11
    Tr_P = P[0, 0] + P[1, 1]
    det_P = P[0, 0] * P[1, 1] - P[1, 0] * P[0, 1]
    Delta = Tr_P ** 2. - 4 * det_P
    l1 = (Tr_P - np.sqrt(Delta)) / 2.
    l2 = (Tr_P + np.sqrt(Delta)) / 2.
    #
    roh1 = np.sqrt(abs(r / l2))
    roh2 = np.sqrt(abs(r / l1))
    return roh1, roh2
    #


def proj_prll_static__(r1, r2, r3, higher_limit_angle=1):
    """
    Compute the parallel projection of an ellipsoid (r1,r2,r3) for a random orientation (P,T,R)
    @param r1: major semi-axe
    @param r2: first minor semi-axe
    @param r3: second major semi-axe
    @return: projected axes rho_1 and rho_2
    """
    # Ellipsoid aligned on the axis:
    AA = np.zeros([3, 3])
    AA[0, 0] = 1. / r1 ** 2.
    AA[1, 1] = 1. / r2 ** 2.
    AA[2, 2] = 1. / r3 ** 2.
    #
    # Sample rotation along z,y and x (note z is useless -> rotation in the image plane).
    # P = random.random() * 2 * np.pi # z-axis rotation
    # P = 0
    # Exact formula for Theta is:
    # T= np.arccos(1 - 2*random.random()) # - np.pi / 2.
    # -> [- pi/2, pi/2]
    # But with symmetry we have:
    T = np.arccos(random.uniform(0, higher_limit_angle)) - np.pi / 2.
    T = abs(T)
    # -> [0, pi/2]
    #
    R = random.random() * 2 * np.pi  # x-axis rotation
    # Rotation matrix:
    # rot = rot_x(R) @ rot_y(T) @ rot_z(P) # (useless)
    rot = rot_x(R) @ rot_y(T)

    #
    # A = np.transpose(rot).dot(AA).dot(rot)
    A = np.transpose(rot) @ AA @ rot

    A11 = A[:2, :2]
    A21 = A[2, :2].reshape(1, 2)
    r = - A[2, 2]
    A21_T = np.transpose(A21)
    # P = A21_T.dot(A21) + r * A11
    P = A21_T @ A21 + r * A11
    Tr_P = P[0, 0] + P[1, 1]
    det_P = P[0, 0] * P[1, 1] - P[1, 0] * P[0, 1]
    Delta = Tr_P ** 2. - 4 * det_P
    l1 = (Tr_P - np.sqrt(Delta)) / 2.
    l2 = (Tr_P + np.sqrt(Delta)) / 2.
    #
    roh1 = np.sqrt(abs(r / l2))
    roh2 = np.sqrt(abs(r / l1))
    return roh1, roh2


def proj_perspective_static(r1, r2, r3, delta, eps):
    """
    Compute the perspective projection of an ellipsoid (r1,r2,r3) for a random orientation (P,T,R)

    Parameters:
    -----------
    @param eps: ellipsoid-view point length, epsilon
    @param delta: ellipsoid-projection plane length
    @param r1: major semi-axe
    @param r2: first minor semi-axe
    @param r3: second minor semi-axe

    Returns:
    --------
    @return: roh1, roh2: projected axes
    """
    AA = np.zeros([3, 3])
    AA[0, 0] = 1. / r1 ** 2.
    AA[1, 1] = 1. / r2 ** 2.
    AA[2, 2] = 1. / r3 ** 2.
    #
    P = random.random() * 2 * np.pi
    T = np.arccos(1 - 2 * random.random()) - np.pi / 2.
    R = random.random() * 2 * np.pi
    #
    rot = rot_z(P).dot(rot_y(T)).dot(rot_x(R))
    A = np.transpose(rot).dot(AA).dot(rot)
    A11 = A[:2, :2]
    A21 = A[2, :2].reshape(1, 2)
    a33 = A[2, 2]
    A21_T = np.transpose(A21)
    r = - (1 - delta / eps) ** 2. * a33
    Q = 2 * (eps - delta) / eps ** 2. * A21
    P = A21_T.dot(A21) - (a33 - 1 / eps ** 2.) * A11
    C = - np.linalg.inv(P).dot(np.transpose(Q)) / 2.
    Delta = np.trace(P) ** 2. - 4 * np.linalg.det(P)
    l1 = (np.trace(P) - np.sqrt(Delta)) / 2.
    l2 = (np.trace(P) + np.sqrt(Delta)) / 2.
    roh1 = abs((r - Q.dot(C) / 2.) / l2) ** .5
    roh2 = abs((r - Q.dot(C) / 2.) / l1) ** .5
    return roh1[0, 0], roh2[0, 0]


class Projection(object):
    """
    Simulate the projection of a set of N ellipsoids.

    Parameters:
    -----------
    N: number of ellipsoids to simulate
    """

    def __init__(self, N=1000):
        self.N = N

    #
    def proj_set_scale_invariant(self, r2_pdf, r3_pdf, higher_limit_angle=1, r1_pdf_shape=None, r1_pdf=None):
        """
        Compute a set of projected semi axes from a set of ellipsoids with shapes defined from the input PDFs and
        for a random and uniform orientation for each.
        Values are stored in self.Rho1 and self.Rho2.
        No arguments are returned.
        @param alpha_pdf: str, pdf of alpha to use 'uniform', 'exp' or 'normal'
        @param scaling: bool, to use a scaled or not scaled version
        @param r2_pdf: first minor semi-axe
        @param r3_pdf: second major semi-axe
        @param higher_limit_angle: this parameter allow to bias the orientation PDF. Should not be used for uniform
        approximation.
        """
        if (higher_limit_angle < 0) or (higher_limit_angle > 1):
            raise ValueError(f"higher_limit_angle must be between 0 and 1 (include), got {higher_limit_angle} instead.")
        #
        if higher_limit_angle == 0:
            warnings.warn("higher_limit_angle is set to zero."
                          "This means there is no rotation allowed along the y-axis")
        elif higher_limit_angle < 1:
            warnings.warn("higher_limit_angle is different than one."
                          "This means the orientation uniformity is not respected.")
        #
        if r1_pdf_shape == 'uniform':
            r_1 = np.random.uniform(1,10 , size=self.N)
        elif r1_pdf_shape == 'exp':
            # r_1 = np.random.exponential(scale=0.43, size=self.N) + 1
            r_1 = np.random.exponential(scale=0.5, size=self.N)
        elif r1_pdf_shape == 'normal':
            r_1 = abs(np.random.normal(3, 0.5, size=self.N))
        elif r1_pdf_shape=='kde':
            r_1 = abs(r1_pdf.resample(self.N)[0] - 0.5) + 0.5
            r_1[r_1>6] = 6
        elif r1_pdf_shape=='poly':
            poly = [-0.1544473,   1.4523751 , -5.77856693,15.52607841]
            r1_range = np.linspace(0.5, 6, 10000)
            r1_pdf = np.exp(np.poly1d(poly)(r1_range))
            r_1 = np.random.choice(r1_range, p=r1_pdf / r1_pdf.sum(), size=self.N)

        elif r1_pdf_shape == None:
            r_1 = np.ones(self.N)
        else:
            raise NotImplementedError(f"r1_pdf_shape must be None, 'uniform', 'exp' or 'normal', got {r1_pdf} instead")

        r_2_sampled = r2_pdf.resample(self.N)[0]
        r_3_sampled = r3_pdf.resample(self.N)[0]
        #
        r_2 = np.ndarray([self.N])
        r_3 = np.ndarray([self.N])
        #
        # r_2 = np.random.uniform(5, 15, size=self.N)
        # r_3 = np.random.uniform(1, 2, size=self.N)
        #
        Rho1 = []
        Rho2 = []
        Vr = []
        print("Starting projections for all ellipsoids :")
        for i in tqdm(range(self.N)):
            r_2[i] = np.random.choice(r_2_sampled)
            r_3[i] = np.random.choice(r_3_sampled)
            # To ensure r2 >= r3 (not optimal -> FIXME):
            while r_3[i] > r_2[i]:
                r_2[i] = np.random.choice(r_2_sampled)
                r_3[i] = np.random.choice(r_3_sampled)
            #
            Vr.append(4. / 3. * np.pi * r_1[i] * r_2[i] * r_3[i] * r_1[i] ** 2)  # Real volume
            (roh1, roh2) = proj_prll_static(r_1[i], r_2[i] * r_1[i], r_3[i] * r_1[i],
                                            higher_limit_angle=higher_limit_angle)
            Rho1 += [roh1]
            Rho2 += [roh2]
        # store results:
        # -- ground truth:
        # self.alpha = alpha
        self.Rho1 = np.array(Rho1)
        self.Rho2 = np.array(Rho2)
        self.Vr = np.array(Vr)
        self.r1 = r_1
        self.r2 = r_2
        self.r3 = r_3
        # -- volume estimations from ESD and ELL:
        self.V_ESD = 4. / 3. * np.pi * (self.Rho1 * self.Rho2) ** (3. / 2.)
        self.V_Ell = 4. / 3. * np.pi * self.Rho1 * self.Rho2 ** 2.

    def proj_set_hybrid(self, r1_pdf, r2_pdf, r3_pdf, r1_range_limits=(1, 1), higher_limit_angle=1):
        """
        Compute a set of projected semi axes from a set of ellipsoids with shapes defined from the input PDFs and
        for a random and uniform orientation for each.
        Values are stored in self.Rho1 and self.Rho2.
        No arguments are returned.
        @param r2_pdf: first minor semi-axe
        @param r3_pdf: second major semi-axe
        @param higher_limit_angle: this parameter allow to bias the orientation PDF. Should not be used for uniform
        approximation.
        """
        if (higher_limit_angle < 0) or (higher_limit_angle > 1):
            raise ValueError(f"higher_limit_angle must be between 0 and 1 (include), got {higher_limit_angle} instead.")
        # #
        if higher_limit_angle == 0:
            warnings.warn("higher_limit_angle is set to zero."
                          "This means there is no rotation allowed along the y-axis")
        elif higher_limit_angle < 1:
            warnings.warn("higher_limit_angle is different than one."
                          "This means the orientation uniformity is not respected.")
        #
        r1_range = np.linspace(r1_range_limits[0], r1_range_limits[1], r1_pdf.shape[0])
        r_1 = np.random.choice(r1_range, p=r1_pdf / r1_pdf.sum(), size=self.N)
        #
        r_2_sampled = r2_pdf.resample(self.N)[0]
        r_3_sampled = r3_pdf.resample(self.N)[0]
        #
        r_2 = np.ndarray([self.N])
        r_3 = np.ndarray([self.N])
        #
        Rho1 = []
        Rho2 = []
        Vr = []
        print("Starting projections for all ellipsoids :")
        for i in tqdm(range(self.N)):
            r_2[i] = np.random.choice(r_2_sampled) * r_1[i]
            r_3[i] = np.random.choice(r_3_sampled) * r_1[i]
            while r_3[i] > r_2[i]:
                r_2[i] = np.random.choice(r_2_sampled) * r_1[i]
                r_3[i] = np.random.choice(r_3_sampled) * r_1[i]
            #
            progression(i, self.N)
            Vr.append(4. / 3. * np.pi * r_1[i] * r_2[i] * r_3[i])  # Real volume
            (roh1, roh2) = proj_prll_static(r_1[i], r_2[i], r_3[i], higher_limit_angle=higher_limit_angle)
            Rho1 += [roh1]
            Rho2 += [roh2]
        print("\n")
        # store results:
        # -- ground truth:
        self.Rho1 = np.array(Rho1)
        self.Rho2 = np.array(Rho2)
        self.Vr = np.array(Vr)
        self.r1 = r_1
        self.r2 = r_2
        self.r3 = r_3
        # -- volume estimations from ESD and ELL:
        self.V_ESD = 4. / 3. * np.pi * (self.Rho1 * self.Rho2) ** (3. / 2.)
        self.V_Ell = 4. / 3. * np.pi * self.Rho1 * self.Rho2 ** 2.

    #
    def proj_set_3D(self, r1_pdf, r2_pdf, r3_pdf, r1_range_limits=(1, 1), higher_limit_angle=1):
        """
        Compute a set of projected semi axes from a set of ellipsoids with shapes defined from the input PDFs and
        for a random and uniform orientation for each.
        Values are stored in self.Rho1 and self.Rho2.
        No arguments are returned.

        Parameters:
        -----------
        @param r1_pdf: major semi-axe
        @param r2_pdf: first minor semi-axe
        @param r3_pdf: second minor semi-axe
        @param r1_range_limits: r1 range limits
        @param lenth_cste: True if r1=1 (default is False). If True, r2 and r3 must be a scipy.stats.kde.gaussian_kde
        object
        @param higher_limit_angle: this parameter allow to bias the orientation PDF. Should not be used for uniform
        approximation.

        Returns:
        --------
        None
        """
        r1_range = np.linspace(r1_range_limits[0], r1_range_limits[1], r1_pdf.shape[0])
        r_1 = np.random.choice(r1_range, p=r1_pdf / r1_pdf.sum(), size=self.N)
        #
        r_2 = np.ones([self.N]) * r_1
        r_3 = np.ones([self.N]) * r_1
        mu_2 = r2_pdf[0] * r_1 + r2_pdf[1]
        mu_3 = r3_pdf[0] * r_1 + r3_pdf[1]
        print("Start sampling r2 and r3 (r1 >= r2 >= r3)")
        for i in tqdm(range(self.N)):
            # progression(i, self.N)
            # To ensure r1 >= r2 >= r3 (not optimal -> FIXME):
            while r_2[i] >= r_1[i]:  #
                r_2[i] = np.random.normal(mu_2[i], 0.05 * r_1[i], 1)
            while r_3[i] >= r_2[i]:
                r_3[i] = np.random.normal(mu_3[i], 0.05 * r_1[i], 1)
        #
        Rho1 = []
        Rho2 = []
        Vr = []
        print("Starting projections for all ellipsoids :")
        for i in range(self.N):
            progression(i, self.N)
            Vr += [(4. / 3.) * np.pi * r_1[i] * r_2[i] * r_3[i]]  # Real volume
            (roh1, roh2) = proj_prll_static(r_1[i], r_2[i], r_3[i],
                                            higher_limit_angle=higher_limit_angle)  # compute proj
            Rho1 += [roh1]
            Rho2 += [roh2]
        print("\n")
        # Keep results in memory:
        self.Rho1 = np.array(Rho1)
        self.Rho2 = np.array(Rho2)
        # Volume estimations:
        self.V_Ell = 4. / 3. * np.pi * self.Rho1 * self.Rho2 ** 2.
        self.V_ESD = 4. / 3. * np.pi * (self.Rho1 * self.Rho2) ** (3. / 2.)
        # Real values:
        self.Vr = np.array(Vr)
        self.r1 = r_1
        self.r2 = r_2
        self.r3 = r_3
    #
