# Copyright UCA/CNRS/Inria
# Contributor(s): Cedric Dubois 2022
#
# cedric.dubois@inria.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#
import numpy as np
import matplotlib.pyplot as plt
import pandas
import os
import zooplankton
from utils import progression
from joblib import Parallel, delayed
import math
import glob

#
class DataSet(object):
    '''
    This class allow to load and make data treatment on different datasets.
    For now it is optimize for copepods images from EcoTaxa only.
    '''

    def __init__(self, name, data_path, meta_data_filepath):
        """
        This class manage all the dataset.
        @param name: dataset name
        @param data_path: path of the directory containing all the data
        @param meta_data_filepath: The file exported from EcoTaxa, aontaning metadata (.tsv or .csv)
        """
        self.ds_name = name
        self.data_path = data_path
        self.meta_data_filepath = meta_data_filepath
    #
    def _load_copepods(self,labels=None,learn_data_path=None,lowmem=True):
        """
        Load copepods images and associated metadata
        @param labels: If None all metadata (i.e. columns in the metafile) are load,
        else provide a list of metadata labels (default is None).
        @param learn_data_path:
        @param lowmem: Boolean. If True the images (original and processed) will not be stored. If False, all images
        of each image processing steps will be store. This is done in the XXXX method.
        """
        # If the copepod list (numpy ndarray) is already available, load it. ELse create it
        if os.path.isfile(self.data_path + '/copepods_list.npy') is False:
                if self.meta_data_filepath[-3:] == 'csv':
                    sep = ','
                elif self.meta_data_filepath[-3:] == 'tsv':
                    sep = '\t'
                self.meta_file = pandas.read_csv(self.meta_data_filepath, sep=sep, encoding ="ISO-8859-1")
                self.nb_data = self.meta_file.shape[0]
                dt = np.dtype(zooplankton.Copepod)  # copepod type for numpy ndarray
                self.copepods_list = []
                print(f"Loading all the {self.nb_data} data \n Progression :")
                # First load the meta data
                if labels is None:
                    self.labels = [lab for lab, cont in self.meta_file.iteritems()] # clolumns names
                else:
                    self.labels = labels
                #
                # learn_imgs = []
                # if learn_data_path != None:
                #     folds = glob.glob(learn_data_path + "*")
                #     for i in folds:
                #         file_dir = glob.glob(i + '/*')
                #         for file in file_dir:
                #             learn_imgs += [file.split('/')[-1]]
                # Then load all the images
                for j in range(0, self.nb_data):
                        progression(j,self.nb_data)
                        if (self.meta_file.object_annotation_category[j]=='Copepoda') and \
                                (self.meta_file.object_annotation_status[j]=='validated') : #and \
                                # not any(self.meta_file['img_file_name'][j].split("/")[-1] in x
                                #          for x in learn_imgs):
                            fp = self.data_path + str(self.meta_file['img_file_name'][j]) #+ exten
                            if labels is None:
                                if 'side' in self.meta_file['img_file_name'][j]:
                                    cop = zooplankton.Copepod(file_path=fp, meta_data=self.meta_file, id=j,
                                                              annotation='side')
                                elif 'top' in self.meta_file['img_file_name'][j]:
                                    cop = zooplankton.Copepod(file_path=fp, meta_data=self.meta_file, id=j,
                                                              annotation='top-bottom')
                                else:
                                    cop = zooplankton.Copepod(file_path=fp, meta_data=self.meta_file, id=j,
                                                              annotation='unknown')
                            else:
                                if 'side' in self.meta_file['img_file_name'][j]:
                                    cop = zooplankton.Copepod(file_path=fp, meta_data=self.meta_file, id=j,
                                                              annotation='side', labels=self.labels)
                                elif 'top' in self.meta_file['img_file_name'][j]:
                                    cop = zooplankton.Copepod(file_path=fp, meta_data=self.meta_file, id=j,
                                                              annotation='top-bottom', labels=self.labels)
                                else:
                                    cop = zooplankton.Copepod(file_path=fp, meta_data=self.meta_file, id=j,
                                                              annotation='unknown', labels=self.labels)
                            cop.load()
                            if lowmem:
                                cop.fit_ellipse_lowmem()
                            else:
                                cop.fit_ellipse()
                            cop.fit_ellipse_zooprocess()
                            cop.compute_volume()
                            self.copepods_list += [cop]
                self.nb_data = len(self.copepods_list)
            #np.save(self.data_path + './copepods_list.npy', self.copepods_list)
        else:
            self.copepods_list = np.load(self.data_path + './copepods_list.npy')
            self.nb_data = self.copepods_list.shape[0]
    #
    def load_copepods(self,legend=True,labels=None,lowmem=True,save_name=False):
        """
        Load copepods images and associated metadata
        @param labels: If None all metadata (i.e. columns in the metafile) are load,
        else provide a list of metadata labels (default is None).
        @param learn_data_path:
        @param lowmem: Boolean. If True the images (original and processed) will not be stored. If False, all images
        of each image processing steps will be store. This is done in the XXXX method.
        """
        # If the copepod list (numpy ndarray) is already available, load it. ELse create it
        if os.path.isfile(self.data_path + str(save_name)) is False:
                if self.meta_data_filepath[-3:] == 'csv':
                    sep = ','
                elif self.meta_data_filepath[-3:] == 'tsv':
                    sep = '\t'
                self.meta_file = pandas.read_csv(self.meta_data_filepath, sep=sep, encoding ="ISO-8859-1")
                self.nb_data = self.meta_file.shape[0]
                self.copepods_list = []
                print(f"Loading all the {self.nb_data} data \n Progression :")
                # First load the meta data
                if labels is None:
                    self.labels = [lab for lab, cont in self.meta_file.iteritems()] # clolumns names
                else:
                    self.labels = labels
                #
                for j in range(0, self.nb_data):
                        progression(j,self.nb_data)
                        if (self.meta_file.object_annotation_category[j]=='Copepoda') and \
                                (self.meta_file.object_annotation_status[j]=='validated') :
                            fp = self.data_path + str(self.meta_file['img_file_name'][j])
                            # if labels is None:
                            #     cop = copepod.Copepod(file_path=fp, meta_data=self.meta_file, id=j,
                            #                               orient='unknown', legend=legend)
                            # else:
                            cop = zooplankton.Copepod(file_path=fp, meta_data=self.meta_file, id=j,
                                                      annotation='unknown', labels=self.labels, legend=legend)
                        #
                            cop.load()
                            if lowmem:
                                cop.fit_ellipse_lowmem()
                            else:
                                cop.fit_ellipse()
                            cop.fit_ellipse_zooprocess()
                            cop.compute_volume()
                            self.copepods_list += [cop]
                        #
                        elif (self.meta_file.object_annotation_category[j]=='top-bottom') and \
                                (self.meta_file.object_annotation_status[j]=='validated') :
                            fp = self.data_path + str(self.meta_file['img_file_name'][j])
                            # if labels is None:
                            #     cop = copepod.Copepod(file_path=fp, meta_data=self.meta_file, id=j,
                            #                               orient='top-bottom', legend=legend)
                            # else:
                            cop = zooplankton.Copepod(file_path=fp, meta_data=self.meta_file, id=j,
                                                      annotation='top-bottom', labels=self.labels, legend=legend)
                        #
                            cop.load()
                            if lowmem:
                                cop.fit_ellipse_lowmem()
                            else:
                                cop.fit_ellipse()
                            cop.fit_ellipse_zooprocess()
                            cop.compute_volume()
                            self.copepods_list += [cop]
                        #
                        elif (self.meta_file.object_annotation_category[j]=='side') and \
                                (self.meta_file.object_annotation_status[j]=='validated') :
                            fp = self.data_path + str(self.meta_file['img_file_name'][j])
                            # if labels is None:
                            #     cop = copepod.Copepod(file_path=fp, meta_data=self.meta_file, id=j,
                            #                               orient='side', legend=legend)
                            # else:
                            cop = zooplankton.Copepod(file_path=fp, meta_data=self.meta_file, id=j,
                                                      annotation='side', labels=self.labels, legend=legend)
                        #
                            cop.load()
                            if lowmem:
                                cop.fit_ellipse_lowmem()
                            else:
                                cop.fit_ellipse()
                            cop.fit_ellipse_zooprocess()
                            cop.compute_volume()
                            self.copepods_list += [cop]
                        else:
                            print(f"Not selected copepod: id={j} \n "
                                  f"object_annotation_category={self.meta_file.object_annotation_category[j]},"
                                  f"\n object_annotation_status[j]={self.meta_file.object_annotation_status[j]}"
                                  f"\n img filepath = {self.data_path + str(self.meta_file['img_file_name'][j])}"
                                  f"------------------------------------------------------------------------------")
                        #
                        #
                self.nb_data = len(self.copepods_list)
                #
                if save_name is not False:
                    print(f"Saving copepod list as {self.data_path + save_name}")
                    dt = np.dtype(zooplankton.Copepod)
                    np.save(self.data_path + save_name, np.array(self.copepods_list,dtype=dt))
        else:
            self.copepods_list = np.load(self.data_path + save_name,allow_pickle=True)
            self.nb_data = self.copepods_list.shape[0]
            if self.meta_data_filepath[-3:] == 'csv':
                sep = ','
            elif self.meta_data_filepath[-3:] == 'tsv':
                sep = '\t'
            self.meta_file = pandas.read_csv(self.meta_data_filepath, sep=sep, encoding="ISO-8859-1")
            # Load the meta data
            if labels is None:
                self.labels = [lab for lab, cont in self.meta_file.iteritems()]  # clolumns names
            else:
                self.labels = labels
    #
    def load_copepods_no_meta(self):
        """
        Load copepods images without associated metadata
        """
        # If the copepod list (numpy ndarray) is already available, load it. ELse create it
        if os.path.isfile(self.data_path + '/copepods_list.npy') is False:
            # If all images in the same folder
                G = glob.glob(self.data_path + '*')
                self.nb_data = len(G)
                dt = np.dtype(zooplankton.Copepod)  # copepod type for numpy ndarray
                self.copepods_list = np.ndarray([self.nb_data], dtype=dt)
                print(f"Loading all the {self.nb_data} data \n Progression :")
                for j in range(0, self.nb_data):
                    progression(j,self.nb_data)
                    img = plt.imread(G[j])
                    # If The image is normalized, extend to 255
                    if img.max() < 1.1:
                        img *= 255.
                    else:
                        # If it is normalized, remove the legend :
                        img = img[:-31, :, 0]
                    cop = zooplankton.Copepod(file_path=G[j], img=img, meta_data=None, id=j, inpx=True)
                    # cop.load()
                    cop.norm_img()
                    cop.fit_ellipse()
                    cop.fit_ellipse_zooprocess()
                    cop.compute_volume_inpx()
                    self.copepods_list[j] = cop
        else:
            self.copepods_list = np.load(self.data_path + './copepods_list.npy')
            self.nb_data = self.copepods_list.shape[0]
    #
    def load_copepods_prll(self):
        """
        EXPERIMENTAL. DO NOT USE.
        Load copepods images and associated metadata
        """
        # If all images in the same folder
        if self.sub_folders is False:
            self.image_paths = [os.path.join(r, file) for r, d, f in os.walk(self.data_path) for file in f
                                if file[-3:] == 'png'] # r is data_path, file take all file names
            self.nb_data = len(self.image_paths)
            #
            dt = np.dtype(zooplankton.Copepod) # copepod type for numpy ndarray
            self.copepods_list = np.ndarray([self.nb_data], dtype=dt)
            print(f"Loading all the {self.nb_data} data in parallel :")
            # First load the meta data
            #meta_file = np.genfromtxt(self.meta_data_path, delimiter=',', dtype='U120')
            meta_file = pandas.read_csv(self.meta_data_filepath, sep=',')
            labels = [lab for lab, cont in meta_file.iteritems()] # clolumns names
            # Then load all the images in parallel :
            def load(j,meta_file):
                fp = self.data_path + str(meta_file['objid'][j]) + '.png'
                cop = zooplankton.Copepod(file_path=fp, meta_data=meta_file, id=j, large=True)
                cop.load()
                cop.fit_ellipse_lowmem()
                cop.fit_ellipse_zooprocess()
                cop.compute_volume()
                self.copepods_list[j] = cop
            #
            tab_res = Parallel(n_jobs=-1, backend='loky')(delayed(load)(j=j,meta_file=meta_file)
                                                                     for j in range(0, meta_file.shape[0]))
    def load_copepods_for_axes_ratio(self):
        self.meta_file = pandas.read_csv(self.meta_data_filepath, sep='\t')
        self.nb_data = self.meta_file.shape[0]
        dt = np.dtype(zooplankton.Copepod)  # copepod type for numpy ndarray
        self.copepods_list = np.ndarray([self.nb_data], dtype=dt)
        print(f"Loading all the {self.nb_data} data \n Progression :")
        # First load the meta data
        self.labels = [lab for lab, cont in self.meta_file.iteritems()]  # clolumns names
        # Then load all the images
        for j in range(0, self.meta_file.shape[0]):
            progression(j, self.nb_data)
            #fp = self.data_path + str(self.meta_file['objid'][j]) + '.png'
            fp = self.meta_data_filepath[:-37] + self.meta_file['img_file_name'][j]
            # fp = self.meta_data_path[:-23] + 'imagescop/' + meta_file[j,2] + '.png'
            if 'side' in self.meta_file['img_file_name'][j]:
                cop = zooplankton.Copepod(file_path=fp, meta_data=self.meta_file, id=j, large=True, annotation='side')
            elif 'top' in self.meta_file['img_file_name'][j]:
                cop = zooplankton.Copepod(file_path=fp, meta_data=self.meta_file, id=j, large=True, annotation='top-bottom')
            else:
                cop = zooplankton.Copepod(file_path=fp, meta_data=self.meta_file, id=j, large=True, annotation='unknown')
            cop.load()
            # cop.FitEllipse_lowmem()
            cop.fit_ellipse()
            cop.fit_ellipse_zooprocess()
            cop.compute_volume()
            self.copepods_list[j] = cop

    def extract_meta_data(self):
        """
        Extract all the data set meta datas into numpy arrays
        """
        # Meta data of the meta file :
        # for lab, cont in self.meta_file.iteritems():
        for lab in self.labels:
            if lab[-1]=='.':
                var = 'self.' + lab[:-1]
            else:
                var = 'self.' + lab
            # if type(cont[0]) == str:
            if type(self.meta_file[lab][0]) == str:
                exec(f"{var} = np.zeros([self.nb_data], dtype='U120')")
            else:
                exec(f"{var} = np.zeros([self.nb_data])")
        # Fill the meta data of the meta file :
        for i, ii in enumerate(self.copepods_list):
            #for lab, cont in ii.meta_data.iteritems():
            k = 0
            for lab in self.labels:
                if lab[-1] == '.':
                    var = 'self.' + lab[:-1] + '[i]'
                else:
                    var = 'self.' + lab + '[i]'
                if type(self.meta_file[lab][ii.id]) == str:
                    exec(f"{var} = '{ii.meta_data[lab][ii.id]}'")
                elif math.isnan(self.meta_file[lab][ii.id]):
                    exec(f"{var} = 'nan'")
                else:
                    exec(f"{var} = {ii.meta_data[lab][ii.id]}")
            ii.meta_data = math.nan
                # Retrive real pixel size :
                # TODO : Harmonize pixel label
    def extract_cop_data(self):
        """
        Extract all cops specs computed into numpy array
        """
        # New parameters to extract
        [self.Maj, self.Min, self.Angle, self.Center_x, self.Center_y, self.V, self.V_bin, self.Area, self.Area_bin,
         self.V_ESD, self.V_ESD_bin, self.V_EcoTaxa, self.pixel, self.pixel_raw, self.hd, self.Lat, self.Lon,
         self.Depth,self.Min_bin,self.Maj_bin] = \
            [np.zeros([self.nb_data]) for i in range(20)]
        #
        self.orient = np.ndarray([self.nb_data],dtype='U10')
        #
        for i, ii in enumerate(self.copepods_list):
            if ii.inpx is True:
                self.pixel[i] = self.pixel_raw[i] = 1.
                self.hd[i] = False
            else:
                if ii.px_size >= 0.1:
                    self.pixel[i] = ii.px_size / 2.
                    self.hd[i] = False
                else:
                    self.pixel[i] = ii.px_size
                    self.hd[i] = True
            # Additional :
            #self.name[i] = ii.name
            #self.Valid_EcoTaxa[i] = ii.Valid_EcoTaxa
            #self.Maj_EcoTaxa[i] = ii.Maj_EcoTaxa
            #self.Min_EcoTaxa[i] = ii.Min_EcoTaxa
            #self.Angle_EcoTaxa[i] = ii.Angle_EcoTaxa
            #self.X_EcoTaxa[i] = ii.X_EcoTaxa
            #elf.Y_EcoTaxa[i] = ii.Y_EcoTaxa
            #self.Xm_EcoTaxa[i] = ii.Xm_EcoTaxa
            #self.Ym_EcoTaxa[i] = ii.Ym_EcoTaxa
            #self.Lat_EcoTaxa[i] = ii.Lat_EcoTaxa
            #self.Lon_EcoTaxa[i] = ii.Lon_EcoTaxa
            #self.D_min_EcoTaxa[i] = ii.D_min_EcoTaxa
            #self.D_max_EcoTaxa[i] = ii.D_max_EcoTaxa
            #
            #
            self.orient[i] = ii.annotation
            # Fill the new parameters extracted
            self.Maj[i] = ii.major
            self.Min[i] = ii.minor
            self.Angle[i] = ii.orientation
            self.Center_x[i] = ii.BC[1]
            self.Center_y[i] = ii.BC[0]
            #
            # assert isinstance(ii.V, object)
            self.V[i] = ii.V
            self.V_ESD[i] = ii.V_ESD
            self.Area[i] = ii.Area
            # assert isinstance(ii.V_EcoTaxa, object)
            self.V_EcoTaxa[i] = ii.V_EcoTaxa
            #
            self.Min_bin[i] = ii.minor_zp
            self.Maj_bin[i] = ii.major_zp
            self.V_bin[i] = ii.V_bin
            self.V_ESD_bin[i] = ii.V_ESD_bin
            self.Area_bin[i] = ii.Area_bin
            #
            self.Lat[i] = ii.object_lat
            self.Lon[i] = ii.object_lon
            self.Depth[i] = ii.object_depth_max


            #
            #self.pixel[i] = ii.pixel
            #self.pixel_raw[i] = ii.pixel_raw
            #self.hd[i] = ii.hd
        #
        # self.Mask = np.where((self.V >= 0.1))[0]
        self.Mask = np.where((self.V>0.1) & (self.V_bin>0.1) & (self.V_ESD>0.1)& (self.V_ESD_bin>0.1))[0]
    #
    #
    def save_results(self,path):
        """
        Save the most important quantities into numpy binaries.
        """
        if not os.path.isdir(path):
            os.makedirs(path)
        #
        np.save(path+"V_ELL",self.V)
        np.save(path+"V_ESD",self.V_ESD)
        np.save(path+"V_ELL_zp",self.V_bin)
        np.save(path+"V_ESD_zp",self.V_ESD_bin)
        np.save(path+"Area",self.Area)
        np.save(path+"Area_zp",self.Area_bin)
        np.save(path+"ESD",2*np.sqrt(self.Area/np.pi))
        np.save(path+"ESD_zp",2*np.sqrt(self.Area_bin/np.pi))
        np.save(path+"Maj",self.Maj)
        np.save(path+"Maj_zp",self.Maj_bin)
        np.save(path + "Min", self.Min)
        np.save(path + "Min_zp", self.Min_bin)
        np.save(path+"Lat",self.Lat)
        np.save(path+"Lon",self.Lon)
        np.save(path+"Depth",self.Depth)
        np.save(path+"Pixel",self.pixel)
        np.save(path+"Pixel_raw",self.pixel_raw)

