# Copyright UCA/CNRS/Inria
# Contributor(s): Cedric Dubois 2022
#
# cedric.dubois@inria.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#
import warnings
import math
#
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
#
from scipy.ndimage import distance_transform_edt
#
from skimage.measure import regionprops, label
from skimage.segmentation import clear_border


class Copepod():
    '''
    Load and process copepod images (individual or full data-set)
    '''

    def __init__(self, file_path, meta_data, id=0, annotation='unknown',
                 inpx=False, invert=True, legend=True, labels=None):
        """
        @param file_path: str, file path to the image
        @param meta_data: pandas.core.frame.DataFrame, dataframe of the meta data from Eco-Taxa export
        @param id: int, for identification
        @param annotation (optional): str, precise annotation, can be useful to deal with different populations
        @param inpx(optional): bool, to work with pixel and no pixel size
        @param invert(optional): bool, whether to invert image
        @param legend(optional): bool, whether to remove legend on image or not
        @param labels(optional): None or list, list of meta data labels to load ; if None all labels are loaded
        """
        self.file_path = file_path  # path of the image
        self.meta_data = meta_data
        self.id = id
        self.Flag_img = False
        self.annotation = annotation
        self.inpx = inpx
        self.invert = invert
        self.legend = legend
        if inpx is True:
            self.px_size = self.sample_pixel = 1.
        self.labels = labels

    #
    def norm_img(self):
        if self.legend is True:
            self.img = self.img[:-31, :, 0]
        #
        if self.invert is True:
            if self.img.max() < 1.1:
                self.img *= 255.
            self.img = (255 - self.img) / 255.
        else:
            if self.img.max() > 1.:
                self.img /= 255.

    #
    def load(self):
        """
        Do not use if ne metafile is import
        or
        if you want result in px
        """
        if self.labels is None:
            self.labels = [lab for lab, cont in self.meta_data.iteritems()]  # clolumns names
        #     for lab, cont in self.meta_data.iteritems():
        #         if lab[-1] == '.':
        #             var = 'self.' + lab[:-1]
        #         else:
        #             var = 'self.' + lab
        #         if type(cont[self.id]) == str:
        #             exec(f"{var} = '{self.meta_data[lab][self.id]}'")
        #         elif math.isnan(cont[self.id]):
        #             exec(f"{var} = 'nan'")
        #         elif lab == 'object_%area':
        #             self.object_area_pc = self.meta_data[lab][self.id]
        #         else:
        #             exec(f"{var} = {self.meta_data[lab][self.id]}")
        # else:
        for lab in self.labels:
            if lab[-1] == '.':
                var = 'self.' + lab[:-1]
            else:
                var = 'self.' + lab
            if type(self.meta_data[lab][self.id]) == str:
                exec(f"{var} = '{self.meta_data[lab][self.id]}'")
            elif math.isnan(self.meta_data[lab][self.id]):
                exec(f"{var} = 'nan'")
            elif lab == 'object_%area':
                self.object_area_pc = self.meta_data[lab][self.id]
            else:
                exec(f"{var} = {self.meta_data[lab][self.id]}")
        # Retrive real pixel size :
        try:
            if self.sample_uvpmodel == 'HD':
                self.px_size = self.sample_pixel
                self.hd = True
            else:
                self.px_size = self.sample_pixel / 2.
                self.hd = False
        except AttributeError:
            if self.sample_model == 'HD':
                self.sample_pixel = self.px_size
                self.hd = True
            else:
                self.sample_pixel = self.px_size
                self.px_size /= 2.
                self.hd = False
        #
        # try:
        #     if self.sample_pixel >= 0.1:
        #         self.px_size = self.sample_pixel / 2.
        #         self.hd = False
        #     else:
        #         self.px_size = self.sample_pixel
        #         self.hd = True
        # except AttributeError:
        #     if self.px_size >= 0.1:
        #         self.sample_pixel = self.px_size
        #         self.px_size /= 2.
        #         self.hd = False
        #     else:
        #         self.sample_pixel = self.px_size
        #         self.hd = True
        #
        if self.inpx is True:
            self.px_size = 1.
            self.sample_pixel = 1.
            self.hd = False
        #
        self.img = plt.imread(self.file_path)
        self.norm_img()

    #
    def fit_ellipse_lowmem(self):
        """
        Low Memory usage (no plots availables)
        input : image from EcoTaxa of dim (sizeX,sizeY,3)
        Compute an ellipse fit on the isolated and binarized body of a copepod.
        The distance map (compute on the binarized image) allow to isolate the body
        from the antennas and the tail, with a threshold.
        return : major, minor, angle, center
        """
        # Flags :
        self.F_border = False
        self.F_zeros = False
        self.input_binary_mask = self.img.copy()
        # Binarize :
        self.bin_threshold = 0.03  # 0.01 TODO
        self.input_binary_mask[self.input_binary_mask < self.bin_threshold] = 0
        self.input_binary_mask[self.input_binary_mask >= self.bin_threshold] = 1
        # Compute Distance map :
        D = distance_transform_edt(self.input_binary_mask)
        self.eroded_dist_threshold = D.max() * 0.5238
        D2 = D.copy()
        D2[D2 <= self.eroded_dist_threshold] = 0
        D2[D2 > self.eroded_dist_threshold] = 1
        #
        D4 = distance_transform_edt(1 - clear_border(D2))
        D4[D4 <= self.eroded_dist_threshold] = 1
        D4[D4 > self.eroded_dist_threshold] = 0
        D4 = clear_border(D4)
        if D4.sum() == 0.:  # If there was only one object on the border
            self.F_zeros = True
            self.F_border = True
            self.major = 0
            self.minor = 0
            self.orientation = 0
            self.BC = [0, 0]
            self.V_ESD = 0
            self.area = 0
        else:
            # Classical (binary) ellipse fit on the isolated body :
            regions = regionprops(label(D4.astype('int64'), return_num=False, connectivity=1))
            k = 0
            while len(regions) == 0:
                k += 1
                if k > 3:
                    print("Error Fit Ellipse, k>3")  # TODO
                    print(D4.sum())
                    print(self.id)
                    raise ValueError(f"Can not find reasonable body for copepod id {self.id} "
                                     f"\n file_path: {self.file_path}")
                self.eroded_dist_threshold -= 0.5
                D2 = D.copy()
                D2[D2 <= self.eroded_dist_threshold] = 0
                D2[D2 > self.eroded_dist_threshold] = 1
                #
                D4 = distance_transform_edt(1 - clear_border(D2))
                D4[D4 <= self.eroded_dist_threshold] = 1
                D4[D4 > self.eroded_dist_threshold] = 0
                D4 = clear_border(D4)
                if D4.sum() == 0.:  # If there is no more object
                    self.F_zeros = True
                    self.F_border = True
                    self.major = 0
                    self.minor = 0
                    self.orientation = 0
                    self.BC = [0, 0]
                    warnings.warn(f"Can not find object in copepod image id {self.id} of filepath {self.file_path}."
                                  f"putting 0 for all values.")
                #
                regions = regionprops(label(D4.astype('int64'), return_num=False, connectivity=1))
            # List of all the connexe regions :
            self.Air = [region.area for region in regions]
            # Select the larger region :
            props = regions[int(np.argmax(self.Air))]
            self.area = props.area
            self.BC = props.centroid  # Center (on binary body)
            self.orientation = np.rad2deg(props.orientation) + 90.  # Orientation (on binary body), force to add 90
            self.major = props.major_axis_length  # + 2 * self.ss
            self.minor = props.minor_axis_length  # + 2 * self.ss

    #
    def fit_ellipse(self, th=0):
        """
        see Supporting Information S3 for details.
        input : image from EcoTaxa of dim (sizeX,sizeY,3)
        Compute an ellipse fit on the isolated and binarized body of a copepod.
        The distance map (compute on the binarized image) allow to isolate the body
        from the antennas and the tail, with a threshold.
        """
        # Flags :
        self.F_border = False
        self.F_zeros = False
        self.input_binary_mask = self.img.copy()
        # Binarize :
        self.bin_threshold = 0.03  # 0.01 # empirical value
        self.input_binary_mask[self.input_binary_mask < self.bin_threshold] = 0
        self.input_binary_mask[self.input_binary_mask >= self.bin_threshold] = 1
        # self.img3 = self.img3[minx-5:maxx+5,miny-5:maxy+5]
        # self.img = self.img[minx-5:maxx+5,miny-5:maxy+5]
        # Compute Distance map :
        self.inner_dist_map = distance_transform_edt(self.input_binary_mask)
        # Binarize i.e. remove the pixel close to the border of the mask
        regions = regionprops(label(self.input_binary_mask.astype('int64'), return_num=False, connectivity=1))
        props = regions[int(np.argmax([region.area for region in regions]))]
        miny, minx, maxy, maxx = props.bbox
        self.max_box = max(maxx - minx, maxy - miny)
        if th == 0:
            self.eroded_dist_threshold = self.inner_dist_map.max() * 0.5238  # empirical value
        else:
            self.eroded_dist_threshold = self.inner_dist_map.max() - th
        #
        self.eroded_dist_map = self.inner_dist_map.copy()
        self.eroded_dist_map[self.eroded_dist_map <= self.eroded_dist_threshold] = 0
        self.eroded_dist_map[self.eroded_dist_map > self.eroded_dist_threshold] = 1
        self.clean_eroded_dist_map = clear_border(self.eroded_dist_map)  # Clear objects connected to the image border
        #
        self.outer_dist_map = distance_transform_edt(1 - self.clean_eroded_dist_map)  # outer dist map
        self.dilated_body = self.outer_dist_map.copy()
        self.dilated_body[self.dilated_body <= self.eroded_dist_threshold] = 1
        self.dilated_body[self.dilated_body > self.eroded_dist_threshold] = 0
        self.dilated_body = clear_border(self.dilated_body)
        if self.dilated_body.sum() == 0.:  # If there was only one object and it was connected to the border
            self.F_zeros = True
            self.F_border = True
            self.major = 0
            self.minor = 0
            self.orientation = 0
            self.BC = [0, 0]
            self.V_ESD = 0
            self.area = 0
        else:
            # Classical (binary) ellipse fit on the dilated body :
            regions = regionprops(label(self.dilated_body.astype('int64'), return_num=False, connectivity=1))
            k = 0
            # To manage cases with no more object remaining due to the erosion (small bodies):
            while len(regions) == 0:
                k += 1
                if k > 3:
                    plt.figure()
                    plt.imshow(self.clean_eroded_dist_map)
                    print(self.clean_eroded_dist_map.sum())
                    plt.show()
                    raise ValueError(f"Can not find reasonable body for copepod id {self.id} "
                                     f"\n file_path: {self.file_path}")
                self.eroded_dist_threshold -= 0.5  # re-do the process with a smaller threshold at each k
                self.eroded_dist_map = self.inner_dist_map.copy()
                self.eroded_dist_map[self.eroded_dist_map <= self.eroded_dist_threshold] = 0
                self.eroded_dist_map[self.eroded_dist_map > self.eroded_dist_threshold] = 1
                self.clean_eroded_dist_map = clear_border(self.eroded_dist_map)
                #
                self.dilated_body = distance_transform_edt(1 - self.clean_eroded_dist_map)
                self.dilated_body[self.dilated_body <= self.eroded_dist_threshold] = 1
                self.dilated_body[self.dilated_body > self.eroded_dist_threshold] = 0
                self.dilated_body = clear_border(self.dilated_body)
                if self.clean_eroded_dist_map.sum() == 0.:  # If there is no more object
                    self.F_zeros = True
                    self.F_border = True
                    self.major = 0
                    self.minor = 0
                    self.orientation = 0
                    self.BC = [0, 0]
                    warnings.warn(f"Can not find object in copepod image id {self.id} of filepath {self.fp}."
                                  f"putting 0 for all values.")

                #
                regions = regionprops(label(self.dilated_body.astype('int64'), return_num=False, connectivity=1))
            # List of all the connected components :
            self.Air = [region.area for region in regions]
            # Select the larger region :
            props = regions[int(np.argmax(self.Air))]
            self.area = props.area
            self.BC = props.centroid  # Center (on binary body)
            self.orientation = np.rad2deg(props.orientation) + 90.  # Orientation (on binary body), force to add 90
            self.major = props.major_axis_length
            self.minor = props.minor_axis_length

    def fit_ellipse_zooprocess(self):
        """
        Compute an ellipse fit on the mask of the copepod.
        This is equivalent to the ZooProcess ellipse fit method.
        """
        # Classical binary ellipse fit :
        regions = regionprops(label(self.input_binary_mask.astype('int64'), return_num=False, connectivity=1))
        # List of all the connected components :
        self.Air_bin = [region.area for region in regions]
        props = regions[int(np.argmax(self.Air_bin))]
        self.area_bin = props.area
        self.centroid_zp = props.centroid  # Center (on binary body)
        self.orientation_zp = np.rad2deg(props.orientation) + 90.  # Orientation (on binary body), force to add 90.
        # Area normalization (scaling that reduce fitting error due to antennas):
        A = (props.major_axis_length / 2. * props.minor_axis_length / 2.) / (self.area_bin / np.pi)
        self.major_zp = np.sqrt(props.major_axis_length ** 2. / A)
        self.minor_zp = np.sqrt(props.minor_axis_length ** 2. / A)

    #
    def compute_volume(self):
        self.V = 4. / 3. * np.pi * self.minor * self.minor * self.major * (self.px_size / 2.) ** 3.
        self.V_ESD = 4. / 3. * np.pi * (self.px_size ** 2. * self.area / np.pi) ** (3. / 2.)
        try:
            self.V_EcoTaxa = 4. / 3. * np.pi * self.minor * self.minor * self.major * (self.sample_pixel / 2.) ** 3.
        except AttributeError:
            self.V_EcoTaxa = 4. / 3. * np.pi * self.object_minor * self.object_minor * self.object_major * (
                    self.sample_pixel / 2.) ** 3.
        except AttributeError:
            warning.warn(
                "No EcoTaxa metadata to compute the volume from ELL (i.e. minor/major or object_minor/object_major).")
            self.V_EcoTaxa = 0.
        self.Area = self.area * self.px_size ** 2.
        #
        self.V_bin = 4. / 3. * np.pi * self.minor_zp * self.minor_zp * self.major_zp * (self.px_size / 2.) ** 3.
        self.V_ESD_bin = 4. / 3. * np.pi * (self.px_size ** 2. * self.area_bin / np.pi) ** (3. / 2.)
        self.Area_bin = self.area_bin * self.px_size ** 2.

    def compute_volume_inpx(self):
        self.V = 4. / 3. * np.pi * self.minor * self.minor * self.major * (1. / 2.) ** 3.
        self.V_ESD = 4. / 3. * np.pi * (self.area / np.pi) ** (3. / 2.)
        self.Area = self.area
        #
        self.V_EcoTaxa = 0.

    #
    def show(self, ax, ell_edge_color='g', zp_ell_edge_color='r', cmap='gray', legend=False, fontsize=15):
        """
        Plot image of the object and fitted ellipses (proposed vs ZooProcess).
        Automatic crop around the bounding box is performed.
        @param ax: matplotlib.axes._subplots.AxesSubplot, axis to plot
        @param ell_edge_color: str, edge color of the fitted ellipse (proposed method)
        @param zp_ell_edge_color: str, edge color of the fitted ellipse (ZooProcess)
        @param cmap: str, colormap for image
        @param legend: bool, to show legend or not
        @param fontsize: int, fontsize
        @return: None
        """
        #
        # Automatic crop :
        regions = regionprops(label(self.input_binary_mask.astype('int64'), return_num=False, connectivity=1))
        props = regions[int(np.argmax([region.area for region in regions]))]
        miny, minx, maxy, maxx = props.bbox
        #
        e = Ellipse(xy=(self.BC[1], self.BC[0]), width=self.major, height=self.minor, angle=-self.orientation,
                    edgecolor=ell_edge_color,
                    fill=0, linewidth=2, label='Proposed')
        #
        eb = Ellipse(xy=(self.centroid_zp[1], self.centroid_zp[0]), width=self.major_zp, height=self.minor_zp,
                     angle=-self.orientation_zp, edgecolor=zp_ell_edge_color, fill=0, label="ZooProcess", linewidth=2)
        #
        ax.imshow(self.img, cmap=cmap)
        ax.add_artist(e)
        ax.add_artist(eb)
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        #
        if self.img.shape[1] <= maxx + 5:
            maxx = self.img.shape[1] - 5
        if minx < 5:
            minx = 5
        ax.set_xlim([minx - 5, maxx + 5])
        if self.img.shape[0] <= maxy + 5:
            maxy = self.img.shape[0] - 5
        if miny < 5:
            miny = 5
        ax.set_ylim([miny - 5, maxy + 5])
        #
        if legend:
            ax.plot(0, 0, c=ell_edge_color, label='Proposed', alpha=1.)
            ax.plot(0, 0, c=zp_ell_edge_color, label='ZooProcess', alpha=1.)
            ax.legend(fontsize=fontsize)

    #
    def show_zooprocess(self, ax, cmap='gray'):
        e = Ellipse(xy=(self.BC[1], self.BC[0]), width=self.major, height=self.minor, angle=-self.orientation,
                    edgecolor='r',
                    fill=0)
        #
        # if self.hd is True:
        #     fpx = 1.
        # else:
        #     fpx = 2.
        #
        e_eco = Ellipse(xy=(self.centroid_zp[1], self.centroid_zp[0]), width=self.major_zp,
                        height=self.minor_zp,
                        angle=-self.orientation_zp, edgecolor='g', fill=0)
        ax.imshow(self.img, cmap=cmap)
        ax.add_artist(e)
        ax.add_artist(e_eco)
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

    #
    def show2(self, axes=None, fontsize=10, linewidth=1):
        #
        if axes == None:
            f, ((ax1, ax2, ax3), (ax4, ax5, ax6)) = plt.subplots(nrows=2, ncols=3, dpi=300)
        else:
            ((ax1, ax2, ax3), (ax4, ax5, ax6)) = axes
        # Crop :
        regions = regionprops(label(self.input_binary_mask.astype('int64'), return_num=False, connectivity=1))
        props = regions[int(np.argmax([region.area for region in regions]))]
        miny, minx, maxy, maxx = props.bbox
        #
        e = Ellipse(xy=(self.BC[1], self.BC[0]), width=self.major, height=self.minor, angle=-self.orientation,
                    edgecolor='springgreen',
                    fill=0, label="Proposed", linewidth=linewidth)
        #
        ax1.imshow(self.img, cmap='cividis')
        # e3 = Ellipse(xy=(self.BC[1], self.BC[0]), width=self.major, height=self.minor, angle=-self.orientation,
        #              edgecolor='springgreen',
        #              fill=0, label="Proposed", linewidth=2)
        ax1.add_artist(e)
        ax1.plot(0, 0, c='springgreen', label='Proposed', alpha=1.)
        ax1.plot(0, 0, c='r', label='ZooProcess', alpha=1.)
        eb = Ellipse(xy=(self.centroid_zp[1], self.centroid_zp[0]), width=self.major_zp, height=self.minor_zp,
                     angle=-self.orientation_zp, edgecolor='r', fill=0, label="ZooProcess", linewidth=linewidth)
        ax1.add_artist(eb)
        ax1.get_xaxis().set_visible(False)
        ax1.get_yaxis().set_visible(False)
        ax1.set_title("Input image", fontsize=fontsize)
        ax1.legend(fontsize=fontsize - 5, loc=0)
        ax1.set_xlim([minx - 5, maxx + 5])
        ax1.set_ylim([miny - 5, maxy + 5])
        #
        ax2.imshow(self.input_binary_mask, cmap='gray')
        eb = Ellipse(xy=(self.centroid_zp[1], self.centroid_zp[0]), width=self.major_zp, height=self.minor_zp,
                     angle=-self.orientation_zp, edgecolor='r', fill=0, label="ZooProcess", linewidth=linewidth)
        ax2.add_artist(eb)
        ax2.get_xaxis().set_visible(False)
        ax2.get_yaxis().set_visible(False)
        ax2.set_title("Binary mask", fontsize=fontsize)
        ax2.set_xlim([minx - 5, maxx + 5])
        ax2.set_ylim([miny - 5, maxy + 5])
        #
        ax3.imshow(self.inner_dist_map, cmap='OrRd')
        # ax3.add_artist(e)
        ax3.get_xaxis().set_visible(False)
        ax3.get_yaxis().set_visible(False)
        ax3.set_title("Inner dist. map", fontsize=fontsize)
        ax3.set_xlim([minx - 5, maxx + 5])
        ax3.set_ylim([miny - 5, maxy + 5])
        #
        ax4.imshow(self.eroded_dist_map, cmap='gray')
        # e2 = Ellipse(xy=(self.BC[1], self.BC[0]), width=self.major, height=self.minor, angle=-self.orientation, edgecolor='g',
        #              fill=0, label="Proposed", linewidth=2)
        # ax4.add_artist(e2)
        ax4.get_xaxis().set_visible(False)
        ax4.get_yaxis().set_visible(False)
        ax4.set_title("Eroded dist. map", fontsize=fontsize)
        ax4.set_xlim([minx - 5, maxx + 5])
        ax4.set_ylim([miny - 5, maxy + 5])
        #
        ax5.imshow(self.outer_dist_map, cmap='OrRd')
        ax5.contour(self.eroded_dist_map, levels=[0.5], linewidths=linewidth)
        ax5.get_xaxis().set_visible(False)
        ax5.get_yaxis().set_visible(False)
        ax5.set_title("Outer dist. map", fontsize=fontsize)
        ax5.set_xlim([minx - 5, maxx + 5])
        ax5.set_ylim([miny - 5, maxy + 5])
        #
        ax6.imshow(self.dilated_body, cmap='gray')
        e2 = Ellipse(xy=(self.BC[1], self.BC[0]), width=self.major, height=self.minor, angle=-self.orientation,
                     edgecolor='springgreen',
                     fill=0, label="Proposed", linewidth=linewidth)
        ax6.add_artist(e2)
        ax6.get_xaxis().set_visible(False)
        ax6.get_yaxis().set_visible(False)
        ax6.set_title("Dilated body", fontsize=fontsize)
        ax6.set_xlim([minx - 5, maxx + 5])
        ax6.set_ylim([miny - 5, maxy + 5])
        #
        plt.tight_layout()
        plt.show()

    def show3(self):
        #
        # Crop :
        regions = regionprops(label(self.input_binary_mask.astype('int64'), return_num=False, connectivity=1))
        props = regions[int(np.argmax([region.area for region in regions]))]
        miny, minx, maxy, maxx = props.bbox
        #
        e = Ellipse(xy=(self.BC[1], self.BC[0]), width=self.major, height=self.minor, angle=-self.orientation,
                    edgecolor='g',
                    fill=0, label="Proposed", linewidth=2)
        #
        f, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(3, 2, figsize=(20, 10))
        #
        ax1.imshow(self.img, cmap='cividis')
        e3 = Ellipse(xy=(self.BC[1], self.BC[0]), width=self.major, height=self.minor, angle=-self.orientation,
                     edgecolor='g',
                     fill=0, label="Proposed", linewidth=2)
        ax1.add_artist(e3)
        ax1.plot(0, 0, c='g', label='Proposed', alpha=1.)
        ax1.plot(0, 0, c='r', label='ZooProcess', alpha=1.)
        eb = Ellipse(xy=(self.centroid_zp[1], self.centroid_zp[0]), width=self.major_zp, height=self.minor_zp,
                     angle=-self.orientation_zp, edgecolor='r', fill=0, label="ZooProcess", linewidth=2)
        ax1.add_artist(eb)
        ax1.get_xaxis().set_visible(False)
        ax1.get_yaxis().set_visible(False)
        ax1.set_title("Input Image", fontsize=20)
        ax1.legend(fontsize=15, loc=0)
        ax1.set_xlim([minx - 5, maxx + 5])
        ax1.set_ylim([miny - 5, maxy + 5])
        #
        ax3.imshow(self.inner_dist_map, cmap='OrRd')
        ax3.add_artist(e)
        ax3.get_xaxis().set_visible(False)
        ax3.get_yaxis().set_visible(False)
        ax3.set_title("Distance Map", fontsize=20)
        ax3.set_xlim([minx - 5, maxx + 5])
        ax3.set_ylim([miny - 5, maxy + 5])
        #
        ax4.imshow(self.dilated_body, cmap='gray')
        e2 = Ellipse(xy=(self.BC[1], self.BC[0]), width=self.major, height=self.minor, angle=-self.orientation,
                     edgecolor='g',
                     fill=0, label="Proposed", linewidth=2)
        ax4.add_artist(e2)
        ax4.get_xaxis().set_visible(False)
        ax4.get_yaxis().set_visible(False)
        ax4.set_title("Resulting mask", fontsize=20)
        ax4.set_xlim([minx - 5, maxx + 5])
        ax4.set_ylim([miny - 5, maxy + 5])
        #

        ax2.imshow(self.input_binary_mask, cmap='gray')
        e4 = Ellipse(xy=(self.BC[1], self.BC[0]), width=self.major, height=self.minor, angle=-self.orientation,
                     edgecolor='g',
                     fill=0, label="Proposed", linewidth=2)
        ax2.add_artist(e4)
        ax2.get_xaxis().set_visible(False)
        ax2.get_yaxis().set_visible(False)
        ax2.set_title("Binary Mask", fontsize=20)
        ax2.set_xlim([minx - 5, maxx + 5])
        ax2.set_ylim([miny - 5, maxy + 5])
        #
        ax5.imshow(self.clean_eroded_dist_map, cmap='gray')
        e2 = Ellipse(xy=(self.BC[1], self.BC[0]), width=self.major, height=self.minor, angle=-self.orientation,
                     edgecolor='g',
                     fill=0, label="Proposed", linewidth=2)
        ax5.add_artist(e2)
        ax5.get_xaxis().set_visible(False)
        ax5.get_yaxis().set_visible(False)
        ax5.set_title("Eroded mask", fontsize=20)
        ax5.set_xlim([minx - 5, maxx + 5])
        ax5.set_ylim([miny - 5, maxy + 5])
        #
        ax6.imshow(self.clean_eroded_dist_map, cmap='gray')
        e2 = Ellipse(xy=(self.BC[1], self.BC[0]), width=self.major, height=self.minor, angle=-self.orientation,
                     edgecolor='g',
                     fill=0, label="Proposed", linewidth=2)
        ax6.add_artist(e2)
        ax6.get_xaxis().set_visible(False)
        ax6.get_yaxis().set_visible(False)
        ax6.set_title("Eroded mask", fontsize=20)
        ax6.set_xlim([minx - 5, maxx + 5])
        ax6.set_ylim([miny - 5, maxy + 5])
        #
        #
        # alpha = self.Min/self.Maj
        # R = (self.area/np.pi)**.5
        # R1 = R * alpha**.5
        # R2 = R * alpha**(-.5)
        # e5 = Ellipse(xy=[self.BC[1], self.BC[0]], width=2*R2,height=2*R1,angle=-self.Angle,edgecolor='b',fill=0)
        # ax4.add_artist(e5)
        #
        # X = self.img3.shape[1]/2.
        # Y = self.img3.shape[0]/2.
        # if self.hd is True:
        #     fpx = 1.
        # else:
        #     fpx = 2.
        # e6 = Ellipse(xy=[self.object_x*fpx, self.object_y*fpx], width=self.object_major*fpx,
        # height=self.object_minor*fpx, angle=-self.object_angle, edgecolor='orange', fill=0, label="ZooProcess")
        # ax1.add_artist(e6)
        # ax1.get_xaxis().set_visible(False)
        # ax1.get_yaxis().set_visible(False)
        plt.tight_layout()
        plt.show()
