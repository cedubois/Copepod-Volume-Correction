# Copyright UCA/CNRS/Inria
# Contributor(s): Cedric Dubois 2022
#
# cedric.dubois@inria.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.
#
import numpy as np
import pandas
import os
import zooplankton
from utils import progression
import glob


class DatasetLoader(list):
    """
    Dataset Loader.
    """
    def __init__(
                self,
                name: str,
                img_dir: str,
                meta_filepath: str = 'same') -> None:
        """
        @param name: Name of the dataset .
        @param img_dir: path of the directory containing all the images to load.
        @param meta_filepath: path of the file containing all the metadata of the dataset.
        """
        super().__init__()
        self.name = name
        self.img_dir = img_dir
        if meta_filepath == 'same':
            file = glob.glob(img_dir + "*.*sv")

            if len(file) > 1:
                raise NameError("meta_filepath = 'same' but found mor than one metadata file. \n "
                                "Please give the complete metadata file path as meta_filepath argument.")
            self.meta_filepath = img_dir + file[0]
        else:
            self.meta_filepath = meta_filepath

        if not os.path.isdir(self.img_dir) or not os.path.isfile(self.meta_filepath):
            raise NameError(f"Image directory or metadata file do not exist: {self.img_dir} and {self.meta_filepath}")

    def load_images_meta(
            self,
            lowmem: bool = True,
            legend: bool = True,
            ):
        if self.meta_filepath[-3:] == 'csv':
            sep = ','
        elif self.meta_filepath[-3:] == 'tsv':
            sep = '\t'
        else:
            raise NameError(f"{self.meta_filepath} should de .csv or .tsv file")

        self.meta_file = pandas.read_csv(self.meta_filepath, sep=sep, encoding="ISO-8859-1")
        self.nb_data = self.meta_file.shape[0]
        self.labels = [lab for lab, cont in self.meta_file.iteritems()]  # clolumns names
        #
        print(f"Loading all the {self.nb_data} data \n Progression :")
        #
        for j in range(self.nb_data):
            progression(j, self.nb_data)
            if self.meta_file.object_annotation_status[j] == 'validated':
                annot = self.meta_file.object_annotation_category[j]
                file_p = self.img_dir + str(self.meta_file['img_file_name'][j])
                cop = zooplankton.Copepod(file_path=file_p, meta_data=self.meta_file, id=j,
                                          annotation=annot, labels=self.labels, legend=legend)
                cop.load()
                if lowmem:
                    cop.fit_ellipse_lowmem()
                else:
                    cop.fit_ellipse()
                cop.fit_ellipse_zooprocess()
                cop.compute_volume()
                self.append(cop)

    def save_dataset(self,save_name: str="dataset"):
        print(f"Saving dataset (list of copepod.Copepod) as {self.img_dir + save_name}")
        dt = np.dtype(zooplankton.Copepod)
        np.save(self.img_dir + save_name, np.array(self, dtype=dt))

    def load_dataset_from_list(self,file_path: str):
        """
        To Do:
        @param file_path:
        @return:
        """





