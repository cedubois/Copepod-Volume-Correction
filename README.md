# Copepod Volume Correction
---
## Paper: "Correcting estimations of copepod volume from 2-dimensional images"
- paper in *Limnology and Oceanography: Methods* journal. 
Joint work with [Éric Debreuve](https://www.i3s.unice.fr/~debreuve/) and [Jean-Olivier Irisson](https://www.obs-vlfr.fr/~irisson/).
- paper link: [HAL](https://hal.archives-ouvertes.fr/hal-03450006)

## Usage

### Ellipse fitting and surface estimation of copepod body:
- tutorial notebook: `reproductibility_ellipse_fitting.ipynb`

### Computation of the total volume correction factors $`\mathcal{T_\ast}`$ for ESD method ($`\mathcal{M}_{\small ESD}`$) and ELLipsoid method ($`\mathcal{M}_{\small ELL}`$)
- tutorial notebook from measured axes ratios: `Tutorial-compute-total-volume-correction-factors.ipynb`
- :construction: tutorial notebook to measure axes ratios from images: *soon available*

## Code structure
- `ellipsoid_projection.py`: simulation of the projection (perspective or parallel) of a set of 3-D ellipsoids 
in a 2-D plane with random and uniform (or not) 3-D rotations. True and estimated volume ($`\mathcal{M}_{\small ESD}`$ 
and $`\mathcal{M}_{\small ELL}`$) are stored for each ellipsoid. The total volume correction factors $`\mathcal{T_\ast}`$ 
are then computed.
- `zooplankton.py`: manage copepod images extracted from [Eco-Taxa](https://ecotaxa.obs-vlfr.fr/) website. Ellipse fitting 
and surface estimation are performed here (proposed and [ZooProcess](https://sites.google.com/view/piqv/softwares/uvp5)). 
An independent function will soon be added for ellipse fitting and surface estimation only.
- `utils.py`: various useful functions.
- :construction: `datasets.py`: manage copepod images data-set extracted from [Eco-Taxa](https://ecotaxa.obs-vlfr.fr/) website.
- :construction: `DatasetLoader.py`: save and load copepod data-sets.

## Author & License
Copyright UCA/CNRS/Inria<br>
Contributor(s): Cedric Dubois 2022

This software is governed by the CeCILL  license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".
